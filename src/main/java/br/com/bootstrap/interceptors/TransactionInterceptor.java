package br.com.bootstrap.interceptors;

import br.com.bootstrap.annotations.Database;
import br.com.bootstrap.annotations.Transactional;
import com.google.common.base.Strings;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Luiz Fernando Alves
 */
@Interceptor
@Transactional
public class TransactionInterceptor {
    
    private final Instance<Session> session;
    private final List<Session> sessions = new ArrayList();

    @Inject
    public TransactionInterceptor(@Any Instance<Session> instanceSession) {
        this.session = instanceSession;
    }

    @AroundInvoke
    public Object aroundInvoke(final InvocationContext context) throws Exception {

        List<Transaction> transactions = new ArrayList();

        try {
            Method method = context.getMethod();
            Transactional transactionalAnnotation = getTransactionalAnnotation(method);
            String[] values = transactionalAnnotation.value();

            for (int i = 0; i < values.length; i++) {

                if (Strings.isNullOrEmpty(values[i])) {
                    transactions.add(this.session.get().beginTransaction());
                } else {
                    Annotation qualifier = new Database.DatabaseLiteral(values[i]);

                    this.sessions.add(this.session.select(qualifier).get());

                    transactions.add(this.sessions.get(i).beginTransaction());
                }
            }

            Object result = context.proceed();

            transactions.stream()
                    .filter((transaction) -> (transaction.isActive()))
                    .forEach(Transaction::commit);

            return result;

        } catch (Exception e) {

            if (transactions.size() > 0) {
                transactions.stream().filter((transaction) -> (transaction != null)).forEach((transaction) -> {
                    transaction.rollback();
                });
            }

            throw e;
        }

    }

    private Transactional getTransactionalAnnotation(Method method) {

        if (method.isAnnotationPresent(Transactional.class)) {
            return method.getAnnotation(Transactional.class);
        } else {
            return method.getDeclaringClass().getAnnotation(Transactional.class);
        }

    }

}
