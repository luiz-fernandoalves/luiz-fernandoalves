package br.com.bootstrap.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;

/**
 *
 * @author luizf
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({TYPE, METHOD, FIELD, PARAMETER})
public @interface Database {

    public String value() default "";

    @SuppressWarnings("serial")
    public static class DatabaseLiteral extends AnnotationLiteral<Database> implements Database {

        private String value;

        public DatabaseLiteral(String value) {
            this.value = value;
        }

        @Override
        public String value() {
            return this.value;
        }

    }

}
