package br.com.bootstrap.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

/**
 *
 * @author luizf
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD,TYPE})
public @interface Transactional {
 
    @Nonbinding
    public String[] value() default "";
    
}
