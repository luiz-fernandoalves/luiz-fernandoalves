package br.com.bootstrap.validators.helpers;

import br.com.bootstrap.validators.models.ValidationMessage;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseValidatorStrategyHelper implements ValidationStrategyHelper {
    
    private final List<ValidationMessage> errors = new ArrayList<>();

    @Override
    public void addError(String type, String description) {
        this.errors.add(new ValidationMessage(type, description));
    }

    @Override
    public List<ValidationMessage> getErrors() {
        return this.errors;
    }
 
}