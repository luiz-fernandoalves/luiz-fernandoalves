package br.com.bootstrap.validators.helpers;

import br.com.bootstrap.validators.models.ValidationMessage;
import java.util.List;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
public interface ValidationStrategyHelper {
    
    public void addError(String type, String description);	

    public List<ValidationMessage> getErrors();
    
}
