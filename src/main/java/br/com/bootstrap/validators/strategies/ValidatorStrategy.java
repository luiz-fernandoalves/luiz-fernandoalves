package br.com.bootstrap.validators.strategies;

import br.com.bootstrap.validators.Validator;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
public abstract class ValidatorStrategy<T> implements ValidationStrategy<T> {
    
    private String message;
    private Object[] defaultParameters;

    public ValidatorStrategy(String message, Object...defaultParameters) {
        this.message = message;
        this.defaultParameters = defaultParameters;
    }

    public ValidatorStrategy<T> message(String message){
        this.message = message;
        return this;
    }
    
    public void addErrors(String type, T t, Validator validator) {
        if(shouldAddError(t)) validator.addError(type, this.message);
    }
    
    protected abstract boolean shouldAddError(T t);
}
