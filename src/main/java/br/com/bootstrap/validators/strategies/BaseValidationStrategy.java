package br.com.bootstrap.validators.strategies;

import java.math.BigDecimal;
import java.util.List;

/**
 * @since 25/06/2018
 * @author Luiz Fernando Alves
 */
public class BaseValidationStrategy {

    protected static final String NOT_EMPTY = "notEmpty";
    protected static final String NOT_NULL = "notNull";
    protected static final String NOT_EMPTY_OR_NULL = "notEmptyorNull";
    protected static final String BIGGER_OR_EQUAL = "biggerOrEqual";
    protected static final String LESS_OR_EQUAL = "lessOrEqual";

    public static ValidatorStrategy<List> notEmpty() {

        return new ValidatorStrategy<List>(NOT_EMPTY) {
            @Override
            public boolean shouldAddError(List list) {
                return list == null || list.isEmpty();
            }
        };

    }

    public static ValidatorStrategy notNull() {
        return new ValidatorStrategy<Object>(NOT_NULL) {

            @Override
            public boolean shouldAddError(Object value) {
                return value == null;
            }
        };
    }

    public static ValidatorStrategy notNullOrEmpty() {
        return new ValidatorStrategy<String>(NOT_EMPTY_OR_NULL) {

            @Override
            public boolean shouldAddError(String value) {
                return value == null || value.isEmpty();
            }
        };
    }

    public static ValidatorStrategy<BigDecimal> biggerOrEqual(final BigDecimal comparer) {

        return new ValidatorStrategy<BigDecimal>(BIGGER_OR_EQUAL) {

            @Override
            public boolean shouldAddError(BigDecimal value) {
                return value == null || value.compareTo(comparer) >= 0;
            }
        };

    }

    public static ValidatorStrategy<BigDecimal> lessOrEqual(final BigDecimal comparer) {

        return new ValidatorStrategy<BigDecimal>(LESS_OR_EQUAL) {

            @Override
            public boolean shouldAddError(BigDecimal value) {
                return value == null || value.compareTo(comparer) <= 0;
            }
        };

    }

}
