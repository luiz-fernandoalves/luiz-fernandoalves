package br.com.bootstrap.validators.strategies;

import br.com.bootstrap.validators.Validator;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
public interface CustomValidationStrategy<T> extends ValidationStrategy<T> {
    
    public void verify(T t, Validator validator);
    
}
