package br.com.bootstrap.validators;

import br.com.bootstrap.validators.exceptions.ValidatorException;
import br.com.bootstrap.validators.helpers.ValidationStrategyHelper;
import br.com.bootstrap.validators.models.ValidationMessage;
import br.com.bootstrap.validators.strategies.CustomValidationStrategy;
import br.com.bootstrap.validators.strategies.ValidatorStrategy;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Validator {
    
    private final ValidationStrategyHelper strategy;
    
    @Inject
    public Validator(ValidationStrategyHelper strategy) {
        this.strategy = strategy;
    }
    
    @XmlTransient
    private Response.Status status = Response.Status.BAD_REQUEST;
     
    
    public void validate() {
        if (hasErrors()) {
            throw new ValidatorException(this);
        }
    }
     
    public <T> Validator verify(String category, T t, ValidatorStrategy<T> validatorStrategy) {
        validatorStrategy.addErrors(category, t, this);
        return this;
    }
    
    public <T> Validator verify(T t, Class<? extends CustomValidationStrategy<T>> clazz) {
        CustomValidationStrategy srtategy = null;
        
        try{
            srtategy = javax.enterprise.inject.spi.CDI.current().select(clazz).get();
        }catch(IllegalStateException ex){ 
           try {
               srtategy = clazz.newInstance();
           } catch (InstantiationException | IllegalAccessException ex1) {}
        }

        srtategy.verify(t, this);
        
        return this;
    }
    
    public Validator withResponseStatus(Response.Status status) {
        
        if (status != null) {
            this.status = status;
        }
        
        return this;
    }
    
    public Validator addError(String category, String message) {
        this.strategy.addError(category, message);
        return this;
    }
    
    public List<ValidationMessage> getErrors() {
        return this.strategy.getErrors();
    } 
    
    public boolean hasErrors() {
        return !this.strategy.getErrors().isEmpty();
    }
    
    public ValidationStrategyHelper getStrategy(){
        return this.strategy;
    }
 
    public Response.Status getStatus() {
        return status;
    }
    
}