package br.com.bootstrap.validators.models;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
public class ValidationMessage {

    private String type;
    private String description;

    public ValidationMessage(String type, String description) {
        this.type = type;
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
