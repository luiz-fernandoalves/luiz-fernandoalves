package br.com.bootstrap.validators.exceptions;

import br.com.bootstrap.validators.Validator;

/**
 *  @since 25/06/2018
 *  @author Luiz Fernando Alves
 */
public class ValidatorException extends RuntimeException {
   
    private final Validator validator;

    public ValidatorException(Validator validator) {
        this.validator = validator;
    }

    public Validator getValidator() {
        return validator;
    }
    
}
