package br.com.bootstrap.validators.exceptions.handlers;

import br.com.bootstrap.validators.Validator;
import br.com.bootstrap.validators.exceptions.ValidatorException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @since 25/06/2018
 * @author Luiz Fernando Alves
 */
@Provider
public class ValidationExceptionHandler implements ExceptionMapper<ValidatorException> {

    @Override
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response toResponse(ValidatorException exception) {
        List<String> errors = new ArrayList<>();

        Validator validator = exception.getValidator();

        if (validator.hasErrors()) {
            validator.getStrategy().getErrors().forEach((error) -> errors.add(error.getDescription())); 
        }

        return Response
                .status(validator.getStatus())
                .type(MediaType.APPLICATION_JSON)
                .entity(validator.getStrategy())
                .header("exception", errors)
                .build();
    }

}
