package br.com.bootstrap.components.filters;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Luiz Fernando Alves
 */
@Provider
public class CharsetRequestFilter implements ContainerRequestFilter {

    private static final String CHARSET_UTF_8 = "charset=UTF-8";
    private static final String CONTENT_TYPE = "Content-Type";

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        MediaType contentType = requestContext.getMediaType();

        if (contentType != null) {
            requestContext.getHeaders().putSingle(CONTENT_TYPE, contentType.toString().concat(";") + CHARSET_UTF_8);
        }

    }

}
